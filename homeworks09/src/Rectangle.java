public class Rectangle extends Figure {
    private double a;
    private double b;

    public Rectangle(int x, int y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        return a * 2 + b * 2;
    }
}
