public class Ellipse extends Figure {
    private double sRadius;
    private double bRadius;

    public Ellipse(int x, int y, double sRadius, double bRadius) {
        super(x, y);
        this.sRadius = sRadius;
        this.bRadius = bRadius;
    }

    public double getPerimeter() {
        return 2 * Math.PI * (Math.sqrt((Math.pow(bRadius, 2) + Math.pow(sRadius, 2)) / 8));
    }
}
