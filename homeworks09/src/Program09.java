public class Program09 {
    public static void main(String[] args) {

        Figure figure = new Figure(10, 15);
        Ellipse ellipse = new Ellipse(0,0,10, 15);
        Rectangle rectangle = new Rectangle(0,0,10, 15);
        Circle circle = new Circle(0,0,10);
        Square square = new Square(0,0,10);

        System.out.println(figure.getPerimeter());
        System.out.println(ellipse.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(circle.getPerimeter());
        System.out.println(square.getPerimeter());
    }

}
